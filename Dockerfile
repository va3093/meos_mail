# client building
FROM node:9.11.1-alpine as build-stage
WORKDIR /client_app/app/client/vue_app

run apk --no-cache add --virtual native-deps \
  g++ gcc libgcc libstdc++ linux-headers make python

COPY . /client_app
RUN npm install
RUN npm run build

RUN apk del native-deps

# Python app
FROM python:3.6-alpine

run apk --no-cache add \
  g++ gcc libgcc libstdc++ linux-headers make

COPY . /app
COPY --from=build-stage /client_app/app/client/vue_app/dist /app/app/client/vue_app/dist

WORKDIR /app

RUN pip install -r requirements.txt


ENTRYPOINT [ "./run_api.sh" ]

