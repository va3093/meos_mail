import typing as t

from .utils import Jsonable
from .managers import (
    InboxesPostfixFileManager,
    AliasesPostfixFileManager
)


class Alias(Jsonable):
    def __init__(self, email_address):
        self.email_address = email_address


class Inbox(Jsonable):
    def __init__(self, email_address: str, aliases: t.List[Alias]=None):
        self.email_address = email_address
        self.aliases = aliases or []

    def add_alias(self, alias: Alias):
        self.aliases.append(alias)

    @classmethod
    def inboxes_from_postfix(
        cls, *, inboxes_path, aliases_path
    ):
        inbox_manager = InboxesPostfixFileManager(
            file_path=inboxes_path
        )
        alias_manager = AliasesPostfixFileManager(
            file_path=aliases_path
        )
        inboxes_index = {}
        for email in inbox_manager.get_email_addresses():
            inboxes_index[email] = Inbox(email_address=email)

        aliases_index = alias_manager.get_aliases()
        for email_address, alias_emails in aliases_index.items():
            for alias in alias_emails:
                inboxes_index[email_address].add_alias(
                    Alias(email_address=alias))
        return list(inboxes_index.values())
