import typing as t

from meos_sdk.app import Response
from meos_sdk.app import MeosAppException
from .models import Inbox
from .constants import MUST_HAVE_ATLEAST_ONE_INBOX
from .managers import (
    InboxesPostfixFileManager,
    AliasesPostfixFileManager
)


def get_inboxes(*, config):
    aliases_file_path = config.get('ALIASES_CONFIG_PATH')
    inboxes_file_path = config.get('EMAIL_CONFIG_PATH')
    inboxes: t.List[Inbox] = Inbox.inboxes_from_postfix(
        inboxes_path=inboxes_file_path,
        aliases_path=aliases_file_path
    )
    inboxes_json = [inbox.to_dict() for inbox in inboxes]
    return Response(
        data=dict(
            inboxes=inboxes_json
        )
    )


def remove_inbox(*, config, inbox):
    aliases_file_path = config.get('ALIASES_CONFIG_PATH')
    inboxes_file_path = config.get('EMAIL_CONFIG_PATH')
    inbox_manager = InboxesPostfixFileManager(
        file_path=inboxes_file_path
    )
    if len(inbox_manager.get_email_addresses()) == 1:
        raise MeosAppException(
            key=MUST_HAVE_ATLEAST_ONE_INBOX
        )
    alias_manager = AliasesPostfixFileManager(
        file_path=aliases_file_path
    )
    inbox_manager.remove_inbox(inbox)
    alias_manager.remove_aliases_referencing_inbox(inbox)
    return get_inboxes(config=config)


def remove_alias(*, config, alias):
    aliases_file_path = config.get('ALIASES_CONFIG_PATH')
    alias_manager = AliasesPostfixFileManager(
        file_path=aliases_file_path
    )
    alias_manager.remove_alias(alias)
    return get_inboxes(config=config)


def add_inbox(*, config, inbox):
    inboxes_file_path = config.get('EMAIL_CONFIG_PATH')
    inbox_manager = InboxesPostfixFileManager(
        file_path=inboxes_file_path
    )
    inbox_manager.add_email_address(inbox)
    return get_inboxes(config=config)


def add_alias(*, config, alias, inbox):
    aliases_file_path = config.get('ALIASES_CONFIG_PATH')
    alias_manager = AliasesPostfixFileManager(
        file_path=aliases_file_path
    )
    alias_manager.add_alias(alias, inbox=inbox)
    return get_inboxes(config=config)
