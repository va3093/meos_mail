from collections import defaultdict


class FileManager:
    def __init__(self, *, file_path):
        self.file_path = file_path

    @property
    def file_contents(self):
        with open(self.file_path) as f:
            return f.read()


class InboxesPostfixFileManager(FileManager):

    def get_email_addresses(self):
        rows = self.file_contents.split('\n')
        cleaned_rows = [row for row in rows if row]
        emails = []
        if all(cleaned_rows):
            for row in cleaned_rows:
                email = row.split('|')[0]
                emails.append(email)
        return emails

    def add_email_address(self, email_address):
        with open(self.file_path, 'a') as f:
            f.write(f'\n{email_address}|{self.get_password_hash()}')
        return self.get_email_addresses()

    def get_password_hash(self):
        rows = self.file_contents.split('\n')
        password_hash = ''
        if rows:
            password_hash = rows[0].split('|')[1]
        return password_hash

    def remove_inbox(self, inbox):
        with open(self.file_path, 'r+') as f:
            f.seek(0)
            for line in self.file_contents.split('\n'):
                if not line.startswith(inbox):
                    f.write(line)
            f.truncate()
        return self.get_email_addresses()


class AliasesPostfixFileManager(FileManager):

    def get_aliases(self):
        rows = self.file_contents.split('\n')
        cleaned_rows = [row for row in rows if row]
        alias_index = defaultdict(list)
        if all(cleaned_rows):
            for row in cleaned_rows:
                split_row = row.split(' ')
                alias_index[split_row[1]].append(split_row[0])
        return alias_index

    def add_alias(self, alias, *, inbox):
        with open(self.file_path, 'a') as f:
            f.write(f'\n{alias} {inbox}')
        return self.get_aliases()

    def remove_alias(self, alias):
        with open(self.file_path, 'r+') as f:
            f.seek(0)
            lines_written = 0
            for line in self.file_contents.split('\n'):
                if not line.startswith(alias):
                    if lines_written == 0:
                        f.write(line)
                    else:
                        f.write(f'\n{line}')
                    lines_written += 1
            f.truncate()
        return self.get_aliases()

    def remove_aliases_referencing_inbox(self, inbox):
        with open(self.file_path, 'r+') as f:
            f.seek(0)
            for line in self.file_contents.split('\n'):
                if not line.strip('\n').endswith(f'{inbox}'):
                    f.write(line)
            f.truncate()
        return self.get_aliases()
