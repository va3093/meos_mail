import datetime
import decimal
import typing as t


class Jsonable(object):
    def _jsonify_value(self, value):
        if (hasattr(value, "to_dict")):
            return value.to_dict()
        elif isinstance(value, datetime.datetime):
            iso = value.isoformat()
            return iso
        elif isinstance(value, (t.List, t.Tuple, t.Set)):
            return [
                self._jsonify_value(item) for item in value]
        elif isinstance(value, decimal.Decimal):
            return str(value)
        else:
            return value

    def to_dict(self):
        dikt = dict()
        for attr, value in self.__dict__.items():
                dikt[attr] = self._jsonify_value(value)
        return dikt
