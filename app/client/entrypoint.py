# from flask import Blueprint, render_template

# client_bp = Blueprint('client_app', __name__,
#                       url_prefix='',
#                       static_url_path='',
#                       static_folder='./vue_app/dist',
#                       template_folder='./vue_app/dist',
#                       )

from meos_sdk.app import Response


def index(path, *, request):
    return Response(file='app/client/vue_app/dist/index.html')
