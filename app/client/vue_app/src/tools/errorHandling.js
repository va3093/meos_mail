import { Notification } from 'element-ui'
import i18n from '@/content'

export const genericErrorNotification = () => {
  Notification({
    type: 'error',
    title: i18n.t('genericError_title'),
    message: i18n.t('genericError_description')
  })
}

export const errorKeysHandler = ({errorKey}) => {
  Notification({
    type: 'error',
    title: i18n.t(`${errorKey}_title`),
    message: i18n.t(`${errorKey}_description`)
  })
}
