import { MessageBox } from 'element-ui'

export const genericConfirmationPrompt = (title, description) => {
  return MessageBox.confirm(
    title, description
  )
}
