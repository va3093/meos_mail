import MeosClient from 'meos-sdk'
import { errorKeysHandler } from './tools/errorHandling'

const API_URL = process.env.VUE_APP_API_URL || `https://${window.location.hostname}`

let meosClient = new MeosClient({ apiUrl: API_URL, errorHandler: errorKeysHandler })

export default {
  getInboxes () {
    return meosClient.call('get_inboxes', {})
  },
  deleteInbox ({ inboxEmail, clearStorage }) {
    return meosClient.call('remove_inbox', { inbox: inboxEmail })
  },
  createInbox ({ inboxEmail }) {
    return meosClient.call('add_inbox', { inbox: inboxEmail })
  },
  deleteAlias ({ aliasEmail }) {
    return meosClient.call('remove_alias', { alias: aliasEmail })
  },
  createAlias ({ aliasEmail, inboxEmail }) {
    return meosClient.call('add_alias', { alias: aliasEmail, inbox: inboxEmail })
  }
}
