import Vue from 'vue'
import App from './App.vue'
import router from './router'
import VueMq from 'vue-mq'
import VueCookies from 'vue-cookies'
import 'vuejs-dialog/dist/vuejs-dialog.min.css'
import axios from 'axios'
import VueAxios from 'vue-axios'
import ElementUI from 'element-ui'
import './styles/element-ui-override.scss'
import locale from 'element-ui/lib/locale/lang/en'
import i18n from './content'

import './filters'

Vue.use(VueAxios, axios)
Vue.use(ElementUI, {locale})
Vue.use(VueCookies)
Vue.use(VueMq, {
  breakpoints: {
    'mobile small': 350,
    mobile: 450,
    tablet: 900,
    laptop: 1250,
    desktop: Infinity
  }
})

Vue.config.productionTip = false

new Vue({
  router,
  i18n,
  render: h => h(App)
}).$mount('#app')
