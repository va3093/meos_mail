from meos_sdk.app import MeosApp, EntryPoint

from app.client.entrypoint import index
from app.api.entry_points import health
from app.inboxes.entry_points import (
    get_inboxes, remove_inbox, remove_alias,
    add_alias, add_inbox
)


app = MeosApp('meos_mail', config_path='app/config.yml')


# Api
app.http_entry_point('/health')(health)


# Inboxes
app.rpc_entry_point()(get_inboxes)
app.rpc_entry_point()(remove_inbox)
app.rpc_entry_point()(remove_alias)
app.rpc_entry_point()(add_alias)
app.rpc_entry_point()(add_inbox)


# Client
app.http_entry_points(
    [EntryPoint('/', defaults={'path': ''}),
     EntryPoint('/client/<path:path>')]
)(index)


app.http_static_folder(
    url_path='', dir_path='app/client/vue_app/dist'
)
