from meos_sdk.app import Response, ResponseStatus


def health(request):
    return Response(status=ResponseStatus.Ok, data=dict(status='ok'))
