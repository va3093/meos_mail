VERSION=`cat VERSION`

migrate:
	FLASK_APP=app flask db migrate

db_init:
	FLASK_APP=app flask db init

db_upgrade:
	FLASK_APP=app flask db upgrade

build:
	docker build --no-cache -t registry.gitlab.com/va3093/meos_website:master .;

run:
	docker run -p 5001:8080 --name api --rm registry.gitlab.com/va3093/meos_website:master

deploy_image: build
	# You will first need to do `docker login registry.gitlab.com`
	docker push registry.gitlab.com/va3093/meos_website:master
	# This is optional
	$(ANSIBLE_DEPLOY_SCRIPT)

run_debug:
	python -m app serve_api

run_local:
	rm -rf tmp;
	mkdir -p tmp;
	echo 'me@wilhelm.meos.app|{SHA512-CRYPT}$6$nGcDiF.MG6PP1fEv$0sd2hlSpmYz0zXUEfRCYiomT.cJJZlyhZfes6MrnmoylYAp/gOtGE7h.D1cpF/K3vS4umbdfwfT8xg.LIyChB.' >> tmp/accounts.cf;
	echo 'alias@wilhelm.meos.app me@wilhelm.meos.app' >> tmp/aliases.cf;
	ALLOW_ALL_CORS=True pipenv run meos_sdk run -a app.app

build_and_run_local:
	python -m devresources build-dev && ALLOW_ALL_CORS=True python -m devresources serve_api

run_local_ui:
	python -m devresources serve_client

npm_install:
	python -m devresources npm_install

run_postgres:
	docker run  \
	--env POSTGRES_PASSWORD=password \
	--env POSTGRES_USER=meos_website \
	--env POSTGRES_DB=meos_website \
	--rm \
	--name postgres \
	-d \
	-p 5432:5432 \
	postgres:9.5

