# Meos App Skeleton

This project provides an initial framework for quickly building an app for the Meos Perosonal Cloud.

# How to use it

1) Great a Gitlab project for your new app

2) Clone the skeleton repo
```
git clone git@gitlab.com:va3093/meos_app_skeleton.git
```

3) Rename the repo and change the origin to point your blank repo
```
mv meos_app_skeleton your_app_name
cd your_app_name
git init
git remote add origin git@gitlab.com:user_name/your_app_name.git
```

4) Create a CI variable `GITLAB_REGISTRY_TOKEN` whose value is your personal access token so that you your app's docker image will be stored in the gitlab registry that is accessible.

# Meos sdk js
When if you want to work on the this project and the meos sdk js at the same time then simple change the `package.json` file to point `meos-sdk` at the local repo for the sdk. eg. `"meos-sdk": "file:../../../../meos-sdk-js"`