from app.inboxes.constants import MUST_HAVE_ATLEAST_ONE_INBOX

DEFAULT_INBOX = 'me@wilhelm.meos.app'
SECOND_INBOX = 'work@wilhelm.meos.app'

DEFAULT_ALIAS = 'alias@wilhelm.meos.app'
SECOND_ALIAS = 'second_alias@wilhelm.meos.app'
THIRD_ALIAS = 'third_alias@wilhelm.meos.app'


def test_get_inboxes(test_app_client):
    # Given
    expected_json = {'inboxes': [
        {
            'email_address': DEFAULT_INBOX,
            'aliases': [{
                'email_address': DEFAULT_ALIAS
            }, {
                'email_address': SECOND_ALIAS
            }]
        },
        {
            'email_address': SECOND_INBOX,
            'aliases': [{
                'email_address': THIRD_ALIAS
            }]
        }
    ]}

    # When
    res = test_app_client.get_inboxes()

    # Then
    assert res.status_code == 200
    assert res.json == expected_json


def test_remove_inboxes(test_app_client):
    # Given
    expected_json = {'inboxes': [
        {
            'email_address': SECOND_INBOX,
            'aliases': [{
                'email_address': THIRD_ALIAS
            }]
        }
    ]}

    # When
    res = test_app_client.remove_inbox(inbox=DEFAULT_INBOX)

    # Then
    assert res.status_code == 200
    assert res.json == expected_json


def test_remove_all_inboxes_throws_error(test_app_client):
    # When
    test_app_client.remove_inbox(inbox=DEFAULT_INBOX)
    res = test_app_client.remove_inbox(inbox=SECOND_INBOX)

    # Then
    assert res.status_code == 422
    assert res.json['error_key'] == MUST_HAVE_ATLEAST_ONE_INBOX


def test_remove_alias(test_app_client):
    # Given
    expected_json = {'inboxes': [
        {
            'email_address': DEFAULT_INBOX,
            'aliases': [{
                'email_address': DEFAULT_ALIAS
            }]
        },
        {
            'email_address': SECOND_INBOX,
            'aliases': [{
                'email_address': THIRD_ALIAS
            }]
        }
    ]}

    # When
    res = test_app_client.remove_alias(alias=SECOND_ALIAS)

    # Then
    assert res.status_code == 200
    assert res.json == expected_json


def test_add_inbox(test_app_client):
    # Given
    new_inbox = 'new@wilhelm@meos.app'
    expected_json = {'inboxes': [
        {
            'email_address': DEFAULT_INBOX,
            'aliases': [{
                'email_address': DEFAULT_ALIAS
            }, {
                'email_address': SECOND_ALIAS
            }]
        },
        {
            'email_address': SECOND_INBOX,
            'aliases': [{
                'email_address': THIRD_ALIAS
            }]
        },
        {
            'email_address': new_inbox,
            'aliases': []
        }
    ]}

    # When
    res = test_app_client.add_inbox(inbox=new_inbox)

    # Then
    assert res.status_code == 200
    assert res.json == expected_json


def test_add_alias(test_app_client):
    # Given
    new_alias = 'new@wilhelm@meos.app'
    expected_json = {'inboxes': [
        {
            'email_address': DEFAULT_INBOX,
            'aliases': [{
                'email_address': DEFAULT_ALIAS
            }, {
                'email_address': SECOND_ALIAS
            }, {
                'email_address': new_alias
            }]
        },
        {
            'email_address': SECOND_INBOX,
            'aliases': [{
                'email_address': THIRD_ALIAS
            }]
        }
    ]}

    # When
    res = test_app_client.add_alias(
        alias=new_alias, inbox=DEFAULT_INBOX)

    # Then
    assert res.status_code == 200
    assert res.json == expected_json
