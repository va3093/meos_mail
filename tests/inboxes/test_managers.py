import pytest

from app.inboxes.managers import (
    InboxesPostfixFileManager, AliasesPostfixFileManager
)


DEFAULT_INBOX = 'me@wilhelm.meos.app'
SECOND_INBOX = 'work@wilhelm.meos.app'

DEFAULT_ALIAS = 'alias@wilhelm.meos.app'
SECOND_ALIAS = 'second_alias@wilhelm.meos.app'
THIRD_ALIAS = 'third_alias@wilhelm.meos.app'


@pytest.fixture
def inbox_manager(tmp_inbox_postfix_file):
    return InboxesPostfixFileManager(
        file_path=tmp_inbox_postfix_file
    )


@pytest.fixture
def alias_manager(tmp_alias_postfix_file):
    return AliasesPostfixFileManager(
        file_path=tmp_alias_postfix_file
    )


def test_get_inbox_email_addresses(
    inbox_manager
):
    # Given
    expected_emails = [DEFAULT_INBOX, SECOND_INBOX]

    # When
    emails = inbox_manager.get_email_addresses()

    # Then
    assert emails == expected_emails


def test_get_aliases(alias_manager):
    # Given
    expected_aliases = {
        DEFAULT_INBOX: [DEFAULT_ALIAS, SECOND_ALIAS],
        SECOND_INBOX: [THIRD_ALIAS]
    }

    # When
    aliases = alias_manager.get_aliases()

    # Then
    assert aliases == expected_aliases


def test_add_inbox_email(inbox_manager):
    # Given
    new_email = 'new@meos.wilhelm.app'
    expected_emails = [DEFAULT_INBOX, SECOND_INBOX, new_email]

    # When
    emails = inbox_manager.add_email_address(new_email)

    # Then
    assert emails == expected_emails


def test_get_password_hash(inbox_manager):
    assert inbox_manager.get_password_hash() == (
        '{SHA512-CRYPT}$6$nGcDiF.MG6PP1fEv$0sd2hlSpmYz0zXUEfRCYiomT'
        '.cJJZlyhZfes6MrnmoylYAp'
        '/gOtGE7h.D1cpF/K3vS4umbdfwfT8xg.LIyChB.'
    )


def test_add_alias(alias_manager):
    # Given
    new_alias = 'new@meos.wilhelm.app'
    expected_aliases = {
        DEFAULT_INBOX: [DEFAULT_ALIAS, SECOND_ALIAS, new_alias],
        SECOND_INBOX: [THIRD_ALIAS]
    }

    # When
    aliases = alias_manager.add_alias(new_alias, inbox=DEFAULT_INBOX)

    # Then
    assert aliases == expected_aliases


def test_remove_inbox_email_addresses(
    inbox_manager
):
    # Given
    expected_emails = [SECOND_INBOX]

    # When
    emails = inbox_manager.remove_inbox(DEFAULT_INBOX)

    # Then
    assert emails == expected_emails


def test_remove_aliases_referencing_inbox(alias_manager):
    # Given
    expected_aliases = {SECOND_INBOX: [THIRD_ALIAS]}

    # When
    aliases = alias_manager.remove_aliases_referencing_inbox(DEFAULT_INBOX)

    # Then
    assert aliases == expected_aliases


def test_remove_aliases(alias_manager):
    # Given
    expected_aliases = {
        DEFAULT_INBOX: [SECOND_ALIAS],
        SECOND_INBOX: [THIRD_ALIAS]
    }

    # When
    aliases = alias_manager.remove_alias(DEFAULT_ALIAS)

    # Then
    assert aliases == expected_aliases
