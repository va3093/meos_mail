from tempfile import NamedTemporaryFile

import pytest
from meos_sdk.testing.client import MeosAppTestClient

from app.app import app as application


@pytest.fixture
def tmp_inbox_postfix_file():
    with open('tests/inboxes/fixtures/mock_postfix-accounts.cf', 'rb') as f:
        file_contents = f.read()
        with NamedTemporaryFile() as temp_f:
            temp_f.write(file_contents)
            temp_f.seek(0)
            yield temp_f.name


@pytest.fixture
def tmp_alias_postfix_file():
    with open('tests/inboxes/fixtures/mock_postfix-aliases.cf', 'rb') as f:
        file_contents = f.read()
        with NamedTemporaryFile() as temp_f:
            temp_f.write(file_contents)
            temp_f.seek(0)
            yield temp_f.name


@pytest.fixture()
def app(tmp_alias_postfix_file, tmp_inbox_postfix_file):
    application.update_config({
        'EMAIL_CONFIG_PATH': tmp_inbox_postfix_file,
        'ALIASES_CONFIG_PATH': tmp_alias_postfix_file,
    })
    return application


@pytest.fixture
def test_app_client(app):
    return MeosAppTestClient(app=app)
