import os
import click
import subprocess

BASE_DIR = os.path.dirname(__file__)
CLIENT_DIR = os.path.join(BASE_DIR, '../app/client', 'vue_app')


@click.group()
def cli():
    """ Flask VueJs Template CLI """
    pass


def _bash(cmd, **kwargs):
    """ Helper Bash Call"""
    click.echo('>>> {}'.format(cmd))
    return subprocess.call(cmd, env=os.environ, shell=True, **kwargs)


@cli.command(help='Run Flask Dev Server')
def serve_api():
    """ Run Flask Development servers"""
    click.echo('Starting Flask dev server...')
    os.environ['ALLOW_ALL_CORS'] = 'True'
    os.environ['OAUTHLIB_INSECURE_TRANSPORT'] = '1'
    cmd = 'python run.py'
    _bash(cmd)


@cli.command(help='Run Vue Dev Server', name='serve_client')
def serve_client():
    """ Run Vue Development Server"""
    click.echo('Starting Vue dev server...')
    cmd = 'npm run serve'
    _bash(cmd, cwd=CLIENT_DIR)


@cli.command(help='Build Vue Application', name='build')
def build():
    """ Builds Vue Application """
    cmd = 'npm run build'
    _bash(cmd, cwd=CLIENT_DIR)
    click.echo('Build completed')


@cli.command(help='Build Vue Application for dev', name='build-dev')
def build_dev():
    """ Builds Vue Application """
    cmd = 'npm run build-dev'
    _bash(cmd, cwd=CLIENT_DIR)
    click.echo('Build completed')


@cli.command(help='Install npm dependencies', name='npm_install')
def npm_install():
    cmd = 'npm i'
    _bash(cmd, cwd=CLIENT_DIR)
    click.echo('Npm dependencies installed')


if __name__ == '__main__':
    cli()
